package com.example.frank.hackathon;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.ArrayMap;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ScrollingActivity extends AppCompatActivity {

    Button btnSubmit;
    PieChart pieChart ;
    ArrayList<Entry> entries ;
    ArrayList<String> PieEntryLabels ;
    PieDataSet pieDataSet ;
    PieData pieData ;
    static DbHelper DbHelper;
    static SQLiteDatabase Db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_v2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        pieChart = (PieChart) findViewById(R.id.chart1);

        entries = new ArrayList<>();

        PieEntryLabels = new ArrayList<String>();

        AddValuesToPIEENTRY();

        //AddValuesToPieEntryLabels();

        pieDataSet = new PieDataSet(entries, "");

        pieData = new PieData(PieEntryLabels, pieDataSet);

        pieDataSet.setColors(ColorTemplate.COLORFUL_COLORS);

        pieDataSet.setValueTextSize(10f);


        pieChart.setData(pieData);

        pieChart.setDescription("Expenditure");

        pieChart.animateY(3000);

        btnSubmit = (Button) findViewById(R.id.btnAddSAM);
        //TODO 3.1 Create a new instance of SpendingDbHelper
        DbHelper = new DbHelper(this);
        //TODO 3.2 Get an instance of the database that can be written to
        Db = DbHelper.getWritableDatabase();
    }

    public void AddValuesToPIEENTRY(){
        try {
            // get JSONObject from JSON file
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            // fetch JSONArray named users
            JSONArray transactionsArray = obj.getJSONArray("transactions");
            // implement for loop for getting users list data
            HashMap<String,Float> mapper = new HashMap<String,Float>();
            for (int i = 0; i < transactionsArray.length(); i++) {

                // create a JSONObject for fetching single user data
                JSONObject transactionDetail = transactionsArray.getJSONObject(i);

                if (mapper.containsKey(transactionDetail.getString("tag"))){ //if the key == tag
                    //mapper.get(transactionDetail.getString("tag")); // old value
                    //Float.valueOf(transactionDetail.getString("amount").trim()).floatValue(); // new value
                    mapper.put(transactionDetail.getString("tag"),mapper.get(transactionDetail.getString("tag"))+Float.valueOf(transactionDetail.getString("amount").trim()).floatValue());
                    continue;
                }
                mapper.put(transactionDetail.getString("tag"),Float.valueOf(transactionDetail.getString("amount").trim()).floatValue());


            }

            int i = 0;
            for (Map.Entry<String, Float> entry : mapper.entrySet()) {
                PieEntryLabels.add(entry.getKey());
                entries.add(new BarEntry(entry.getValue(),i));
                i++;
                System.out.println(entry.getKey());
                System.out.println(entry.getValue());
        }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public String loadJSONFromAsset() {
        String json = null;

        try {
            InputStream is = getAssets().open("getTransaction.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


    public void addSAM(View view) {
        Intent intent = new Intent(this, Main2Activity.class);
        startActivity(intent);

    }
}
