package com.example.frank.hackathon;

import android.provider.BaseColumns;

/**
 * Created by Frank on 26/3/2019.
 */
public class MarkerContract {

    public static final class MarkerEntry implements BaseColumns {

        public static final String TABLE_NAME = "SpendingRecord";
        public static final String COL_CATEGORY = "Category";
        public static final String COL_AMOUNT = "Amount";
        public static final String COL_DATE = "Date";
//        public static final ImageView COL_IMAGE = "image";
    }
}